#ifndef US_DELAY_H
#define US_DELAY_H

#include "main.h"


void us_DELAY_init(void);
void us_DELAY(uint32_t us);

#endif
